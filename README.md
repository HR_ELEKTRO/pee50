# PEE50 - Project Elektrotechniek 5 #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om studiemateriaal voor de cursus "PEE50 - Project Elektrotechniek 5" te verspreiden. 

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/pee50/wiki/).

